import React from 'react';
import ReactDOM from 'react-dom';

import App from './client';

const title = 'Empresas Web';

ReactDOM.render(
  <App title={title} />,
  document.getElementById('app')
);

module.hot.accept();