import React from 'react'

export default props => {
    return (
        <div className="logo-nav">
            <img src={require("../../../resources/img/logo-nav.png")} className="logo_home" />
            <img 
                src={require("../../../resources/img/ic-search-copy.svg")} 
                className="ic_search2" 
                onClick={props.onClickSearch}
            />
        </div>
    )
}