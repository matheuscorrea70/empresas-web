import React, { Component } from 'react'
import SearchForm from '../SearchForm'
import Logo from './Logo'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { searchEnterprise } from '../../App/AppActions'

class TopBar extends Component {

    static mapStateToProps = state => {
        return {
            enterprises: state.app.enterprises,
            headers: state.app.headers
        }
    }

    static mapDispatchToProps = dispatch =>
        bindActionCreators(
            {
                searchEnterprise
            },
            dispatch
        )

    render() {    
        const { enterprises, headers } = this.props
        
        return (
            <nav className="topbar">
                <div className="nav-wrapper">
                    { enterprises === false ? 
                        <Logo onClickSearch={() => this.props.searchEnterprise(headers, '')} /> :
                        <SearchForm />
                    }
                </div>
            </nav>
        )
    }
}

export default connect(
    TopBar.mapStateToProps,
    TopBar.mapDispatchToProps,
)(TopBar)