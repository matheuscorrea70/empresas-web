import React, { Component } from 'react'

export default (props) => {
    return (
        <div className="card horizontal" onClick={props.onClick}>
            <div className="card-image">
                <img src={props.photo} />
            </div>
            <div className="card-stacked">
                <div className="card-content">
                    <div className="text1">{props.text1}</div>
                    <div className="text2">{props.text2}</div>
                    <div className="text3">{props.text3}</div>
                </div>
            </div>
        </div>
    )
}