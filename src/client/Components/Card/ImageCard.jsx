import React, { Component } from 'react'

export default (props) => {
    const { photo, description } = props

    return (
        <div className="card image-card">
            <div className="card-image">
            { photo && <img src={photo} /> }
            </div>
            <div className="card-content">
                <p>{description}</p>
            </div>
        </div>
    )
}