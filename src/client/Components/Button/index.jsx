import React, { Component } from 'react'

export default props => {
    const { label, className, onClick } = props

    return (
        <a className={`waves-effect waves-light btn-large ${className}`} onClick={onClick}>{label}</a>
    )
}