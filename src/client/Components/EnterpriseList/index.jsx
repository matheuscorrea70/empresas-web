import React, { Component } from 'react'
import { connect } from 'react-redux'
import EnterpriseList from './EnterpriseList'
import EnterpriseView from './EnterpriseView'
import { bindActionCreators } from 'redux'
import { selectEnterprise } from '../../App/AppActions'

class EnterprisesContainer extends Component {

    state = {
        selected: ""
    }

    static mapStateToProps = state => {
        return {
            enterprises: state.app.enterprises,
            selected: state.app.selected
        }
    }

    static mapDispatchToProps = dispatch =>
        bindActionCreators(
            {
                selectEnterprise
            },
            dispatch
        )

    render() {    
        const { enterprises, selected } = this.props

        if (enterprises === false)
            return <div className="home-info">Clique na busca para iniciar</div>
        
        return (
            <div>
                {selected === "" ? 
                    <EnterpriseList enterprises={enterprises} onItemClick={this.props.selectEnterprise} /> : 
                    <EnterpriseView enterprise={enterprises[selected]} />
                }
            </div>
        )
    }
}

export default connect(
    EnterprisesContainer.mapStateToProps,
    EnterprisesContainer.mapDispatchToProps
)(EnterprisesContainer)