import React, { Component } from 'react'
import ImageCard from '../Card/ImageCard'
import _ from 'lodash'
import { WS_URL } from '../../Config'


export default props => {
    const description = _.get(props, 'enterprise.description')

    let photo = _.get(props, 'enterprise.photo')

    photo = photo ? `${WS_URL}/${photo}` : null

    return (
        <div>
            <ImageCard photo={photo} description={description} />
        </div>
    )
}