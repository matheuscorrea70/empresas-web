import React, { Component } from 'react'
import HorizontalCard from '../Card/HorizontalCard'
import { WS_URL } from '../../Config'

export default props => {
    const { enterprises } = props

        if (!enterprises)
            return null
        
        return (
            <div>
                {enterprises.map((item, key) => {
                    const typeName = _.get(item, 'enterprise_type.enterprise_type_name')
                    const photo = item.photo ? `${WS_URL}/${item.photo}` : null

                    return (
                        <HorizontalCard 
                            key={key}
                            photo={photo}
                            text1={item.enterprise_name} 
                            text2={typeName} 
                            text3={item.country} 
                            onClick={() => props.onItemClick(key)}
                        />
                    )
                })}
            </div>
        )
}