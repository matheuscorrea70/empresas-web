import React from 'react'

export default props => {
    return (
        <div className="field-container">
            <img className="ic-field" src={require("../../../resources/img/ic-email.svg")} />
            <input type="text" {...props} />
        </div>
    )
}