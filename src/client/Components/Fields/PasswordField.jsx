import React from 'react'

export default props => {
    return (
        <div className="field-container">
            <img className="ic-field" src={require("../../../resources/img/ic-cadeado.svg")} />
            <input type="password" {...props} />
        </div>
    )
}