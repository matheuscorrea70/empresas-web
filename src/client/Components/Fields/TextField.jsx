import React, { Component } from 'react'

export default props => {
    return (
        <input type="text" {...props} />
    )
}