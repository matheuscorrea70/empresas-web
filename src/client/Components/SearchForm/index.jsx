import React, { Component } from 'react'
import TextField from '../Fields/TextField'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { searchEnterprise, clearSearch } from '../../App/AppActions'


class SearchForm extends Component {

    state = {
        text: ""
    }

    static mapStateToProps = state => {
        return {
            headers: state.app.headers
        }
    }

    static mapDispatchToProps = dispatch =>
        bindActionCreators(
            {
                searchEnterprise,
                clearSearch
            },
            dispatch
        )

    handleChange = (e) => {
        this.setState({text: e.target.value})
    }
    
    handleSubmit = (e) => {
        e.preventDefault();

        this.props.searchEnterprise(this.props.headers, this.state.text)
    }

    render() {
        return (
            <div className="search-form">
                <form onSubmit={this.handleSubmit}>
                    <img src={require("../../../resources/img/ic-search-copy.svg")} className="ic_search" onClick={this.handleSubmit} />
                    <TextField 
                        onChange={this.handleChange} 
                        value={this.props.text} 
                        className="search-input"
                        placeholder="Pesquisar"
                    />
                    <img src={require("../../../resources/img/ic-close.svg")} className="ic_close" onClick={this.props.clearSearch} />
                    <input className="btn-submit" type="submit" />
                </form>
            </div>
        )
    }
}

export default connect(
    SearchForm.mapStateToProps,
    SearchForm.mapDispatchToProps
)(SearchForm)