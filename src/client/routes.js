import React from 'react'
import { Route } from 'react-router-dom'

import Home from './Pages/Home'
import Login from './Pages/Login'

export const routes = [
    {
        exact: true,
        path: '/',
        component: Login
    },
    {
        path: '/home',
        component: Home
    }
]

export const renderRoutes = routes => {
    return routes.map((route, i) => <Route key={i} {...route} />)
}

export default routes

// For hot reloading of react components
// Webpack Hot Module Replacement API
if (module.hot) {
    module.hot.accept()
}
