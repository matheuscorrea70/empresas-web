import React, { Component } from 'react'
import TopBar from '../Components/TopBar'
import EnterpriseList from '../Components/EnterpriseList'

export default class Home extends Component {
    render() {    
        return (
            <div>
                <TopBar />
                <div className="home-content">
                    <EnterpriseList />
                </div>
                
            </div>
        )
    }
}