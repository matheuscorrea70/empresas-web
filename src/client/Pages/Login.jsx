import React, { Component } from 'react'
import Button from '../Components/Button'
import EmailField from '../Components/Fields/EmailField'
import PasswordField from '../Components/Fields/PasswordField'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { login } from '../App/AppActions'

class Login extends Component {

    state = {
        email: "testeapple@ioasys.com.br",
        password: "12341234"
    }

    static mapDispatchToProps = dispatch =>
        bindActionCreators(
            {
                login
            },
            dispatch
        )

    handleLogin = () => {
        const { email, password } = this.state

        this.props.login(email, password)
            .then(result => {
                this.props.history.push("/home")
            })
            .catch(() => {

            })
    }

    handleChange = (e) => {
        const name = e.target.name
        const value = e.target.value

        this.setState({ [name]: value })
    }

    render() {
        return (
            <div className="Desktop">
                <div className="login-container">
                    <img src={require("../../resources/img/logo-home.png")} className="logo_home" />
                    <div className="BEM-VINDO-AO-EMPRESA">BEM-VINDO AO EMPRESAS</div>
                    <div className="Lorem-ipsum-dolor-si">Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</div>
                    <div className="fields">
                        <EmailField value={this.state.email} name="email" onChange={this.handleChange} placeholder="E-mail" />
                        <PasswordField value={this.state.password} name="password" onChange={this.handleChange} placeholder="Senha" />
                    </div>
                    <Button className="btn-login" onClick={this.handleLogin} label="Entrar" />
                </div>
            </div>
        )
    }
}

export default connect(
    null,
    Login.mapDispatchToProps
)(Login)