/**
 * Root Reducer
 */

// Import Reducers

import { combineReducers } from 'redux'

import app from './App/AppReducer'

const reducers = {
    app
}

const appReducer = combineReducers(reducers)

// Combine all reducers into one root reducer
const rootReducer = (state, action) => {
    return appReducer(state, action)
}

export default rootReducer
