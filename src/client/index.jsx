import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import routes, { renderRoutes } from './routes'
import { Provider } from 'react-redux'
import { configureStore } from './store'

import '../resources/less/style.less'

class App extends Component {

    render() {        
        const store = configureStore()
        
        return (
            <Provider store={store}>
                <Router>
                    {renderRoutes(routes)}
                </Router>
            </Provider>
        )
    }
}

export default App