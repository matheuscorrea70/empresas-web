import axios from 'axios'
import _ from 'lodash'
import { WS_URL } from '../Config'

export const LOGIN = "APP:LOGIN"
export const SEARCH_ENTERPRISE = "APP:SEARCH_ENTERPRISE"
export const CLEAR_SEARCH = "APP:CLEAR_SEARCH"
export const SELECT_ENTERPRISE = "APP:SELECT_ENTERPRISE"

export function login(email, password) {
    const request = axios.post(`${WS_URL}/api/v1/users/auth/sign_in`, {
        email,
        password
    })
        .then(result => {
            const accessToken = _.get(result, 'headers.access-token', '')
            const uid = _.get(result, 'headers.uid', '')
            const client = _.get(result, 'headers.client', '')

            return {
                "access-token": accessToken,
                uid,
                client
            }
        })

    return {
        type: LOGIN,
        payload: request
    }
}

export function searchEnterprise(headers, name = '') {
    let url = `${WS_URL}/api/v1/enterprises`

    if (name) {
        url += `?name=${name}`
    }

    const request = axios.get(url, { headers: headers })
        .then(result => {
            const enterprises = _.get(result, 'data.enterprises', [])
            
            return enterprises
        })

    return {
        type: SEARCH_ENTERPRISE,
        payload: request
    }
}

export function clearSearch() {
    return {
        type: CLEAR_SEARCH
    }
}

export function selectEnterprise(index) {
    return {
        type: SELECT_ENTERPRISE,
        payload: index
    }
}
