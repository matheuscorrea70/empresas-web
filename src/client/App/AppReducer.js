import {
    LOGIN,
    SEARCH_ENTERPRISE,
    CLEAR_SEARCH,
    SELECT_ENTERPRISE
} from './AppActions'

const initialState = {
    headers: {
        "access-token": "",
        "client": "",
        "uid": ""
    },
    enterprises: false,
    selected: ""
}

const AppReducer = (state = initialState, action) => {
    switch (action.type) {
        
        case LOGIN :
            return { ...state, headers: { ...action.payload } }

        case SEARCH_ENTERPRISE :
            return { ...state, enterprises: [ ...action.payload ], selected: "" }

        case CLEAR_SEARCH :
            return { ...state, enterprises: false, selected: "" }

        case SELECT_ENTERPRISE :
            return { ...state, selected: action.payload }

        default:
            return { ...state }
    }
}

export default AppReducer