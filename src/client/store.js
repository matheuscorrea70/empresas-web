/**
 * Main store function
 */
import { applyMiddleware, compose, createStore } from 'redux'
import promise from 'redux-promise'
import thunk from 'redux-thunk'

import rootReducer from './reducers'

export function configureStore(initialState = {}, middlewares = []) {
    // Middleware and store enhancers
    const enhancers = [applyMiddleware(promise), applyMiddleware(thunk)]

    middlewares.forEach(middleware => {
        enhancers.push(applyMiddleware(middleware))
    })

    const store = createStore(rootReducer, initialState, compose(...enhancers))

    if (module.hot) {
        module.hot.accept('./reducers.js', () => {
            const nextRootReducer = require('./reducers').default
            store.replaceReducer(nextRootReducer)
        })
    }

    return store
}

export default {
    configureStore,
}
